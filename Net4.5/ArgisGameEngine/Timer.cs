﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Argis.Core
{
    public class Timer
    {
        private Stopwatch _timer;
		public long TimeInMs { get { return _timer.ElapsedMilliseconds; } }

		public Timer()
        {
            _timer = new Stopwatch();
        }

        public void Start()
        {
            _timer.Start();
        }

		public void Reset()
		{
			_timer.Restart();
		}

        public bool IsBiggerThan(long time, bool reset = false)
        {
            if(_timer.ElapsedMilliseconds > time)
            {
				if(reset)
				{
					_timer.Reset();
					_timer.Start();
				}
                return true;
            }
            return false;
        }

        public bool IsLessThan(long time, bool reset = false)
        {
            if (_timer.ElapsedMilliseconds < time)
            {
				if (reset)
				{
					_timer.Reset();
					_timer.Start();
				}
				return true;
            }
            return false;
        }
    }
}
