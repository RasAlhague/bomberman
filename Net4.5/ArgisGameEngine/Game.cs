﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Argis.Core
{
    /// <summary>
    /// Klasse die das Konkrete Spiel darstellt. Hier wird das Spiel mit gestartet.
    /// </summary>
    public sealed class Game
    {
        /// <summary>
        /// Die Levels die in dem Spiel verwendet werden sollen.
        /// </summary>
        private readonly List<GameLevel> _gameLevels;
		public int Fps { get; private set; }
		public List<LevelObject> LevelIndependentObjects { get; }
		/// <summary>
		/// Standard
		/// </summary>
		public Game()
        {
            _gameLevels = new List<GameLevel>();
            Console.CursorVisible = false;
			LevelIndependentObjects = new List<LevelObject>();
		}
        /// <summary>
        /// Startet den GameLoop
        /// </summary>
        public void Start()
        {
            GameLoop();
        }
        /// <summary>
        /// Die Spielschleife in der das Spiel abläuft. Wird später stark überarbeitet um auch "GUI-Elemente" zu erlauben,
        /// Die zwischen Level kommen.
        /// </summary>
        private void GameLoop()
        {
            Time.StartGameTime();
            // Gehe alle Levels systematisch durch
            foreach (var level in _gameLevels)
            {
				foreach (var independentObject in LevelIndependentObjects)
				{
					independentObject.SetNewLevel(level);
					level.GameObjects.Add(independentObject);
				}

				level.Start();      // ruft alle Start() methoden der GameObjects auf
                level.Draw();       // ersteinmal die Startposition der Objekte zeichen

				int fps = 0;
				Timer timer = new Timer();
				timer.Start();

				// Eigentlicher Spieleloop
				while (!level.Clear)
                {
					fps++;

                    level.Start();
                    level.Update();                 // Aufruf aller update methoden
                    level.Draw();                   // Aufruf aller draw methoden
                    level.FinalizeIteration();      // Finalisieren bestimmter Aktionen beispielsweise der Move()
                    level.Destroy();                // Zerstören von GameObjects
                    Time.UpdateGameTime();
                    Thread.Sleep(11);    // um die cpu etwas zu entlasten xD
					if (timer.IsBiggerThan(1000, true))
					{
						Fps = fps;
						fps = 0;
					}
				}
				level.DestroyLevel();
			}
			Destroy();
			Time.EndGameTime();
        }
        /// <summary>
        /// Fügt ein Level hinzu
        /// </summary>
        /// <param name="level"></param>
        public void AddLevel(GameLevel level)
        {
            level.Game = this;
            _gameLevels.Add(level);
        }
        /// <summary>
        /// Fügt ein Level an einer Position ein
        /// </summary>
        /// <param name="level"></param>
        /// <param name="pos"></param>
        public void InsertLevel(GameLevel level, int pos)
        {
            level.Game = this;
            _gameLevels.Insert(pos, level);
        }
        /// <summary>
        /// Entfernt ein Level an einer Position
        /// </summary>
        /// <param name="pos"></param>
        public void RemoveAt(int pos)
        {
            _gameLevels.RemoveAt(pos);
        }

		private void Destroy()
		{
			while (LevelIndependentObjects.Count != 0)
			{
				LevelIndependentObjects[0].OnDestroy();
				LevelIndependentObjects.RemoveAt(0);
			}
		}
	}
}