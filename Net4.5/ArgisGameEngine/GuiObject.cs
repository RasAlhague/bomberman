﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Argis.Core
{
	public class GuiObject : GameObject
	{
		public string Outputtext { get; set; }
		public Vector2D TextStartingPostion { get; set; }
		public ConsoleColor TextColor { get; set; }
		public ConsoleColor BackgroundColor { get; set; }
		public GuiObject(int x, int y, GameLevel gameLevel) : base(x, y, gameLevel)
		{
		}
		public override void Start()
		{
			TextColor = ConsoleColor.Gray;
			BackgroundColor = ConsoleColor.Black;
			Outputtext = "";
			WriteUpdate(false);
			_timer.Start();
		}
		public override void Update()
		{
			if (_timer.IsBiggerThan(500, true))
			{
				WriteUpdate(false);
			}
		}
		public override void OnDestroy()
		{
			Console.SetCursorPosition(TextStartingPostion.X, TextStartingPostion.Y);
			Console.BackgroundColor = GameLevel.SkyboxColor;
			Console.ForegroundColor = ConsoleColor.Gray;
			for (int i = 0; i < Outputtext.Length; i++)
			{
				Console.Write(" ");
			}

		}
		public void WriteUpdate(bool fullLine)
		{
			Console.SetCursorPosition(TextStartingPostion.X, TextStartingPostion.Y);
			Console.BackgroundColor = GameLevel.SkyboxColor;
			Console.ForegroundColor = ConsoleColor.Gray;
			for (int i = 0; i < Outputtext.Length; i++)
			{
				Console.Write(" ");
			}
			Console.SetCursorPosition(TextStartingPostion.X, TextStartingPostion.Y);
			Console.BackgroundColor = BackgroundColor;
			Console.ForegroundColor = TextColor;
			if (fullLine)
			{
				Console.WriteLine(Outputtext);
			}
			else
			{
				Console.Write(Outputtext);
			}
		}
	}
}
