﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Argis.Core
{
    /// <summary>
    /// GameObjects, welche in der Console sichbar gezeichnet werden. 
    /// Wenn man keine ansicht und keinen aufruf von <see cref="Draw"/> haben will sollte man
    /// <see cref="GameObject"/> als Basis nehmen. Um ein Objekt in zu Instanzieren muss man immer
    /// eine eigene Klasse von DrawableObject erben lassen.
    /// </summary>
    public abstract class DrawableObject : GameObject
    {
        /// <summary>
        /// Hintergrundfarbe des Objekts
        /// </summary>
        protected ConsoleColor _bgColor;
        /// <summary>
        /// Schriftfarbe des Objektes
        /// </summary>
        protected ConsoleColor _fgColor;
        /// <summary>
        /// Anzeigezeichen des Objektes
        /// </summary>
        protected char _sign;
        public int DrawLayer { get; protected set; }
        /// <summary>
        /// Gibt an ob das DrawableObject wieder gezeichnet werden muss. Sollte möglichst nur nach Bewegungsaktionen aktiviert werden,
        /// da solange NeedsRedraw true ist das Object wieder gezeichnet wird, was in einem Overhead enden kann für die Konsole.
        /// </summary>
        public bool NeedsRedraw { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="gameLevel"></param>
        protected DrawableObject(int x, int y, GameLevel gameLevel)
            : base(x, y, gameLevel)
        {
            NeedsRedraw = true;
            _bgColor = ConsoleColor.Black;
            _fgColor = ConsoleColor.Black;
            _sign = ' ';
        }
        /// <summary>
        /// Wenn das Objekt zerstört wird, übermale das Objekt mit der Skybox Farbe.
        /// </summary>
        public override void OnDestroy()
        {
            DrawPosition(Transform.PreviousX, Transform.PreviousY, GameLevel.SkyboxColor, ConsoleColor.Black, ' ');
        }
        /// <summary>
        /// Zeichnet das <see cref="DrawableObject"/>. Wird nur gezeichnet wenn <see cref="NeedsRedraw"/> true ist.
        /// Wird jedes mal im <see cref="Game.GameLoop"/> aufgerufen.
        /// </summary>
        public virtual void Draw()
        {
            if (NeedsRedraw)
            {
                // Alte Position sauber machen
                DrawPosition(Transform.PreviousX, Transform.PreviousY, GameLevel.SkyboxColor, ConsoleColor.Black, ' ');
                // Neue Position zeichnen
                DrawPosition(Transform.X, Transform.Y, _bgColor, _fgColor, _sign);
                NeedsRedraw = false;    // damit nicht nochmal gezeichnet wird
            }
        }
        /// <summary>
        /// Zeichnet das <see cref="DrawableObject"/> an der angegebenen Position mit den angegebenen werten.
        /// </summary>
        /// <param name="x">X Zeichenposition</param>
        /// <param name="y">Y Zeichenposition</param>
        /// <param name="bgColor">Hintergrundfarbe</param>
        /// <param name="fgColor">Schriftfarbe</param>
        /// <param name="sign">Zeichen</param>
        protected virtual void DrawPosition(int x, int y, ConsoleColor bgColor, ConsoleColor fgColor, char sign)
        {
            Console.SetCursorPosition(x, y);
            Console.ForegroundColor = fgColor;
            Console.BackgroundColor = bgColor;
            Console.Write(sign);
        }
    }
}