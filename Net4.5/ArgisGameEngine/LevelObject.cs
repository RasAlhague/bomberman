﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Argis.Core
{
	public class LevelObject : GameObject
	{
		public bool IsLevelIndependent { get; protected set; }
		public LevelObject(int x, int y, GameLevel gameLevel) : base(x, y, gameLevel)
		{
			IsLevelIndependent = true;
		}
		public override void OnDestroy()
		{
		}
		public override void Start()
		{
		}
		public override void Update()
		{
		}
		public void SetNewLevel(GameLevel level)
		{
			GameLevel = level;
			Collider = new Collider(Transform, level)
			{
				IsEnabled = false
			};
		}
	}
}
