﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Argis.Core
{
    public static class Time
    {
        private static readonly Stopwatch _gameTime = new Stopwatch();
        private static long _lastFrameTime;

        public static long CurrentGameTime
        {
            get
            {
                return _gameTime.ElapsedMilliseconds;
            }
        }

        public static long DeltaTime
        {
            get
            {
                var delta = CurrentGameTime - _lastFrameTime;
                return delta;
            }
        }

        public static void UpdateGameTime()
        {
            _lastFrameTime = CurrentGameTime;
        }

        public static void StartGameTime()
        {
            _gameTime.Start();
        }

        public static void EndGameTime()
        {
            _gameTime.Stop();
        }
    }
}