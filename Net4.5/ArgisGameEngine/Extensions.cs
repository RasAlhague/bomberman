﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Argis.Core
{
	public static class Extensions
	{
		public static Directions Negate(this Directions directions)
		{
			if (directions == Directions.Left)
			{
				return Directions.Right;
			}
			if (directions == Directions.Right)
			{
				return Directions.Left;
			}
			if (directions == Directions.Up)
			{
				return Directions.Down;
			}
			if (directions == Directions.Down)
			{
				return Directions.Up;
			}
			return Directions.None;
		}
	}
}