﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Argis.Core
{
    /// <summary>
    /// Gibt die Richtung an in die sich etwas bewegen soll.
    /// </summary>
    public enum Directions
    {
        Left,
        Right,
        Up,
        Down,
        LeftUp,
        RightUp,
        LeftDown,
        RightDown,
        None
    }
}