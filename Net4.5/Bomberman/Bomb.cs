﻿using Argis.Core;
using System;

namespace Bomberman
{
    public class Bomb : DrawableObject
    {
        public Enemy Origin { get; set; }
        private int counter = 0;
        private Raycast raycast;
		private Timer _bombRedraw;

        public Bomb(int x, int y, GameLevel gameLevel)
            : base(x, y, gameLevel)
        {
            Collider.IsEnabled = true;
            raycast = new Raycast(this);
            DrawLayer = 0;
			_bombRedraw = new Timer();
		}
        public override void Start()
        {
            _bgColor = ConsoleColor.Red;
            _fgColor = ConsoleColor.White;
            _sign = 'B';

            _timer.Start();
			_bombRedraw.Start();
        }
        public override void Update()
        {
			if (_timer.IsBiggerThan(counter))
			{
				counter = counter + 1000;
				_sign = counter.ToString()[0];
				NeedsRedraw = true;
			}
			if (_bombRedraw.IsBiggerThan(500, true))
            {
				_sign = counter.ToString()[0];
                NeedsRedraw = true;
            }
            if (_timer.IsBiggerThan(3000,true))
            {
                SpawnExplosion(Directions.Left, Transform.X - 1, Transform.Y);

                SpawnExplosion(Directions.Right, Transform.X + 1, Transform.Y);

                SpawnExplosion(Directions.Up, Transform.X, Transform.Y - 1);

                SpawnExplosion(Directions.Down, Transform.X, Transform.Y + 1);

                Destroy(this);
            }

        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            if (Origin != null)
            {
                Origin.BombLock = false;
            }

        }

        private void SpawnExplosion(Directions direction, int x, int y)
        {
            Transform transform;
            if (raycast.ShootRay(direction, 1, out transform))
            {
                transform.GameObject.SendMessage("OnReceiveCollision", false, this);

				if(transform.GameObject is Obstacle)
				{
					Explosion explosion = GameLevel.AddNewGameObject<Explosion>(x, y);
					explosion.Direction = direction;
				}
            }
            else
            {
                Explosion explosion = GameLevel.AddNewGameObject<Explosion>(x, y);
                explosion.Direction = direction;
            }
        }


    }
}
