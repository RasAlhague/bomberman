﻿using Argis.Core;
using System;
using System.Linq;

namespace Bomberman
{
    public class Player : Enemy
    {
		private bool _levelCleared;

        public Player(int x, int y, GameLevel gameLevel)
            : base(x, y, gameLevel)
        {
            this.Collider.IsTrigger = false;
        }
        public override void Start()
        {
            base.Start();
            _fgColor = ConsoleColor.DarkGreen;
            _bgColor = ConsoleColor.DarkRed;
            _sign = 'P';
            _scoreValue = 0;
			_levelCleared = false;
        }
        public override void Update()
        {
            if (Input.GetKeyEntered(ConsoleKey.W))
            {
                Transform.Move(Directions.Up, 1);
                NeedsRedraw = true;
            }
            else if (Input.GetKeyEntered(ConsoleKey.A))
            {
                Transform.Move(Directions.Left, 1);
                NeedsRedraw = true;
            }
            else if (Input.GetKeyEntered(ConsoleKey.S))
            {
                Transform.Move(Directions.Down, 1);
                NeedsRedraw = true;
            }
            else if (Input.GetKeyEntered(ConsoleKey.D))
            {
                Transform.Move(Directions.Right, 1);
                NeedsRedraw = true;
            }
            else if (Input.GetKeyEntered(ConsoleKey.R) && !BombLock)
            {
                Bomb bomb = GameLevel.AddNewGameObject<Bomb>(Transform.X, Transform.Y);
                bomb.Origin = this;
                BombLock = true;
                NeedsRedraw = true;
            }

			if(!GameLevel.GameObjects.OfType<Enemy>().Any())
			{
				_levelCleared = true;

				Destroy(this);
			}
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            // ToDo: Next Screen

			if(!_levelCleared)
			{
				GameLevel.Clear = true;
			}
			else
			{
				GameLevel.Clear = true;
			}
        }
    }
}