﻿using Argis.Core;
using System;
using System.IO;

namespace Bomberman
{
	public static class Program
	{
		static void Main(string[] args)
		{
			Argis.Core.LevelCreation.LevelCreator levelCreator = new Argis.Core.LevelCreation.LevelCreator();
			Game game = new Game();

			var levelFiles = Directory.GetFiles("./Level/", "*.lvl");
			var classesFile = Directory.GetFiles("./Level/", "*.cf");

			foreach (var file in levelFiles)
			{
				game.AddLevel(levelCreator.CreateLevelFromFile(file, classesFile[0]));
			}

			game.Start();
		}
	}
}