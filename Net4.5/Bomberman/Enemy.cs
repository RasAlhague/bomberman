﻿using Argis.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bomberman
{
	public class Enemy : DrawableObject
	{
		private readonly Random _r;
		private Raycast _raycast;
		protected int _scoreValue;
		private Pathfinder pathfinder;
		private Transform playerPos;

		public Score Score { get; protected set; }
		public bool BombLock { get; set; }

		public Enemy(int x, int y, GameLevel gameLevel)
			: base(x, y, gameLevel)
		{
			this.Collider.IsEnabled = true;
			this.Collider.IsTrigger = false;
			_raycast = new Raycast(this);
			Score = GameLevel.GameObjects.OfType<Score>().FirstOrDefault();
			_r = new Random();
			DrawLayer = 0;
			pathfinder = new Pathfinder(gameLevel);
		}

		public override void Start()
		{
			_bgColor = ConsoleColor.DarkMagenta;
			_fgColor = ConsoleColor.White;
			BombLock = false;
			_scoreValue = 10;
			_timer.Start();
			playerPos = GameLevel.GameObjects.OfType<Player>().FirstOrDefault().Transform;
			Score = GameLevel.GameObjects.OfType<Score>().FirstOrDefault();
		}

		public override void Update()
		{
			if (_timer.IsBiggerThan(500, true))
			{
				Move();
			}
		}

		public void Move()
		{
			if (LookForBombOrExplosion(out Directions direction, out int distance))
			{
				if (distance < 2)
				{
					if (direction == Directions.Left || direction == Directions.Right)
					{
						int offset = 0;
						AvoidBomb(direction, offset);
					}
					else
					{
						int offset = 2;
						AvoidBomb(direction, offset);
					}
				}
			}
			else
			{
				var directions = GetPreferedDirections(new Vector2D(this.Transform.X, this.Transform.Y), new Vector2D(playerPos.X, playerPos.Y));
				for (int i = 0; i < 4; i++)
				{
					if (directions[i] != Directions.None)
					{
						if (_raycast.ShootRay(directions[i], 1, out Transform transform)						   )
						{
							if((transform.GameObject is Obstacle || transform.GameObject is Player) &&
								pathfinder.CanAvoidBomb(new Vector2D(this.Transform.X, this.Transform.Y), 5))
							{
								// TODO: Hier virtuell in alle richtungen einen bewegen und versuchen nach links oder rechts abzubiegen oder aus reichweite zu gehen
								// das ganze dann als bool zurückgeben und nur wenn true dann bombe legen. Maybe noch eine zusatzfunktion durch die sich er enemy in den nächsten runden nur auf diesen wegen bewegt.
								// Wichtig ist auch bei diesem schritten darauf zu achten das er nicht in eine bombe (oder explosion) rein rennt. maybe noch den bomben time counter öffentlich machen und in bewegungen miteinbeziehen.
								var bomb = GameLevel.AddNewGameObject<Bomb>(Transform.X, Transform.Y);
								bomb.Origin = this;
								BombLock = true;

								return;
							}
							else if(!(transform.GameObject is Wall || transform.GameObject is Enemy))
							{
								Transform.Move(directions[i], 1);
								NeedsRedraw = true;
								return;
							}
						}
						else 
						{
							Transform.Move(directions[i], 1);
							NeedsRedraw = true;
							return;
						}
					}
				}
			}
		}
		private void AvoidBomb(Directions direction, int offset)
		{
			if (!CanMoveVerticalFromBomb(offset))
			{
				Transform.Move(direction.Negate(), 1);
				NeedsRedraw = true;
			}
		}
		private bool CanMoveVerticalFromBomb(int offset)
		{
			for (int i = 0; i < 2; i++)
			{
				var direction = (Directions)(i + offset);
				if (!_raycast.ShootRay(direction, 1, out Transform transform) && LookForObject<Bomb, Explosion>(direction, 5, out int distance))
				{
					this.Transform.Move(direction, 1);
					this.NeedsRedraw = true;
					return true;
				}
			}
			return false;
		}
		public List<Directions> GetPreferedDirections(Vector2D currentPos, Vector2D targetPos)
		{
			int xDist = currentPos.X - targetPos.X;
			int yDist = currentPos.Y - targetPos.Y;
			int absXDist = Math.Abs(xDist);
			int absYDist = Math.Abs(yDist);
			List<Directions> directions = new List<Directions>();
			if (absXDist < absYDist)
			{
				AddXDirection(xDist, directions);
				AddYDirection(yDist, directions);
				directions.Add(directions[0].Negate());
				directions.Add(directions[1].Negate());
			}
			else if (absYDist < absXDist)
			{
				AddYDirection(yDist, directions);
				AddXDirection(xDist, directions);
				directions.Add(directions[0].Negate());
				directions.Add(directions[1].Negate());
			}
			else
			{
				directions.Add(Directions.None);
				directions.Add(Directions.None);
				directions.Add(Directions.None);
				directions.Add(Directions.None);
			}
			return directions;
		}
		private void AddYDirection(int yDist, List<Directions> directions)
		{
			if (yDist > 0)
			{
				directions.Add(Directions.Up);
			}
			else if (yDist < 0)
			{
				directions.Add(Directions.Down);
			}
			else
			{
				directions.Add(Directions.None);
			}
		}
		private void AddXDirection(int xDist, List<Directions> directions)
		{
			if (xDist > 0)
			{
				directions.Add(Directions.Left);
			}
			else if (xDist < 0)
			{
				directions.Add(Directions.Right);
			}
			else
			{
				directions.Add(Directions.None);
			}
		}
		public bool LookForBombOrExplosion(out Directions direction, out int distanceToObject)
		{
			if (LookForObject<Bomb, Explosion>(Directions.Left, 15, out int distance))
			{
				direction = Directions.Left;
				distanceToObject = distance;
				return true;
			}
			else if (LookForObject<Bomb, Explosion>(Directions.Up, 15, out distance))
			{
				direction = Directions.Up;
				distanceToObject = distance;
				return true;
			}
			else if (LookForObject<Bomb, Explosion>(Directions.Right, 15, out distance))
			{
				direction = Directions.Right;
				distanceToObject = distance;
				return true;
			}
			else if (LookForObject<Bomb, Explosion>(Directions.Down, 15, out distance))
			{
				direction = Directions.Down;
				distanceToObject = distance;
				return true;
			}
			else
			{
				direction = Directions.None;
				distanceToObject = distance;
				return false;
			}
		}
		public bool LookForObject<T>(Directions direction, int maxDistance, out int distance)
		{
			if (_raycast.ShootRay(direction, maxDistance, out Transform transform) && transform is T)
			{
				if (direction == Directions.Left || direction == Directions.Right)
				{
					distance = Math.Abs(Transform.X - transform.X);
				}
				else
				{
					distance = Math.Abs(Transform.Y - transform.Y);
				}
				return true;
			}
			distance = 0;
			return false;
		}
		public bool LookForObject<T1, T2>(Directions directions, int maxDistance, out int distance)
		{
			return LookForObject<T1>(directions, maxDistance, out distance) || LookForObject<T2>(directions, maxDistance, out distance);
		}
		public bool LookForObject<T1, T2, T3>(Directions directions, int maxDistance, out int distance)
		{
			return LookForObject<T1>(directions, maxDistance, out distance) || LookForObject<T2>(directions, maxDistance, out distance) || LookForObject<T3>(directions, maxDistance, out distance);
		}

		public virtual void OnReceiveCollision(GameObject other)
		{
			if (other is Explosion)
			{
				Destroy(this);
			}
			else if (other is Bomb)
			{
				Destroy(this);
			}
		}

		public override void OnDestroy()
		{
			base.OnDestroy();

			if (Score != null)
			{
				Score.IncreaseScore(_scoreValue);
			}
		}
	}
}
