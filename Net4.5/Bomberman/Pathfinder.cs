﻿using Argis.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bomberman
{
	public class Pathfinder
	{
		private enum GameObjects
		{
			None,
			Wall,
			Obstacle,
			Player,
			Enemy,
			Bomb,
			Explosion
		}
		private readonly int[,] _virtualLevel;
		private readonly GameLevel _gameLevel;
		private readonly int _rows = 30;
		private readonly int _cols = 100;
		public Pathfinder(GameLevel level)
		{
			_virtualLevel = new int[_rows, _cols];
			_gameLevel = level;
		}
		public void UpdateLevelInformation()
		{
			for (int i = 0; i < _rows; i++)
			{
				for (int y = 0; y < _cols; y++)
				{
					_virtualLevel[i, y] = 0;
				}
			}
			foreach (var gameObject in _gameLevel.GameObjects.OfType<DrawableObject>())
			{
				if (gameObject is Obstacle)
				{
					_virtualLevel[gameObject.Transform.Y, gameObject.Transform.X] = (int)GameObjects.Obstacle;
				}
				else if (gameObject is Wall)
				{
					_virtualLevel[gameObject.Transform.Y, gameObject.Transform.X] = (int)GameObjects.Wall;
				}
				else if (gameObject is Player)
				{
					_virtualLevel[gameObject.Transform.Y, gameObject.Transform.X] = (int)GameObjects.Player;
				}
				else if (gameObject is Enemy)
				{
					_virtualLevel[gameObject.Transform.Y, gameObject.Transform.X] = (int)GameObjects.Enemy;
				}
				else if (gameObject is Bomb)
				{
					_virtualLevel[gameObject.Transform.Y, gameObject.Transform.X] = (int)GameObjects.Bomb;
				}
				else if (gameObject is Explosion)
				{
					_virtualLevel[gameObject.Transform.Y, gameObject.Transform.X] = (int)GameObjects.Explosion;
				}
				else
				{
					_virtualLevel[gameObject.Transform.Y, gameObject.Transform.X] = (int)GameObjects.None;
				}
			}
		}
		public bool CanAvoidBomb(Vector2D startingPosition, int moveDistance)
		{
			for (int i = 0; i < 4; i++)
			{
				Directions direction = (Directions)i;
				if (CheckForObjectInDirection(startingPosition, direction, 4, GameObjects.None))
				{
					return true;
				}
				for (int y = 1; y < moveDistance + 1; y++)
				{
					Vector2D temp = AddMovesInDirection(startingPosition, direction, y);
					for (int z = 0; z < 4; z++)
					{
						Directions lookingDirection = (Directions)z;
						if (CheckVerticalMoves(temp, lookingDirection))
						{
							return true;
						}
					}
				}
			}
			return false;
		}
		private bool CheckVerticalMoves(Vector2D startingPos, Directions dir)
		{
			int offset = 0;
			if (dir == Directions.Left || dir == Directions.Right)
			{
				offset = 2;
			}
			for (int i = 0; i < 2; i++)
			{
				var direction = (Directions)(i + offset);
				if (CheckForObjectInDirection(startingPos, direction, 1, GameObjects.None) &&
				   !CheckForObjectInDirection(startingPos, direction, 4, GameObjects.Bomb) &&
				   !CheckForObjectInDirection(startingPos, direction, 4, GameObjects.Explosion))
				{
					return true;
				}
			}
			return false;
		}
		private Vector2D AddMovesInDirection(Vector2D start, Directions direction, int distance)
		{
			if (direction == Directions.Left)
			{
				start.X -= distance;
			}
			else if (direction == Directions.Right)
			{
				start.X += distance;
			}
			else if (direction == Directions.Up)
			{
				start.Y -= distance;
			}
			else if (direction == Directions.Down)
			{
				start.Y += distance;
			}
			return start;
		}
		private bool CheckForObjectInDirection(Vector2D startingPos, Directions direction, int maxDistance, GameObjects type)
		{
			if (direction == Directions.Down)
			{
				for (int i = 1; i < maxDistance + 1; i++)
				{
					if (_virtualLevel[(startingPos.Y + i), startingPos.X] == (int)type)
					{
						return true;
					}
				}
			}
			else if (direction == Directions.Up)
			{
				for (int i = 1; i < maxDistance + 1; i++)
				{
					if (_virtualLevel[(startingPos.Y - i), startingPos.X] == (int)type)
					{
						return true;
					}
				}
			}
			else if (direction == Directions.Left)
			{
				for (int i = 1; i < maxDistance + 1; i++)
				{
					if (_virtualLevel[startingPos.Y, (startingPos.X - i)] == (int)type)
					{
						return true;
					}
				}
			}
			else if (direction == Directions.Right)
			{
				for (int i = 1; i < maxDistance + 1; i++)
				{
					if (_virtualLevel[startingPos.Y, (startingPos.X + i)] == (int)type)
					{
						return true;
					}
				}
			}
			return false;
		}
	}
}
