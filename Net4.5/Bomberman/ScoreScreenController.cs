﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Argis.Core;

namespace Bomberman
{
	public class ScoreScreenController : GameObject
	{
		private List<Tuple<string, int>> scores;
		private List<GuiObject> _top5;
		private GuiObject _gameOver1;
		private GuiObject _gameOver2;
		private GuiObject _gameOver3;
		private GuiObject _gameOver4;
		private GuiObject _gameOver5;
		private GuiObject _gameOver6;
		private GuiObject _headline;
		private GameController _gameController;
		private int _placing = 0;
		private readonly string _path = @".\scores.sc";

		public ScoreScreenController(int x, int y, GameLevel gameLevel) : base(x, y, gameLevel)
		{
			//_headline = gameLevel.AddNewGameObject<GuiObject>(0, 0);
			//_headline.TextStartingPostion(15)
		}

		public override void OnDestroy()
		{
		}

		public override void Start()
		{
			_top5 = new List<GuiObject>();
			_gameController = GameLevel.GameObjects.OfType<GameController>().FirstOrDefault();
			_gameOver1 = CreateGuiObject(new Vector2D(25, 2), @"  ________                        ________                     ");
			_gameOver2 = CreateGuiObject(new Vector2D(25, 3), @" /  _____/_____    _____   ____   \_____  \___  __ ___________ ");
			_gameOver3 = CreateGuiObject(new Vector2D(25, 4), @"/   \  ___\__  \  /     \_/ __ \   /   |   \  \/ // __ \_  __ \");
			_gameOver4 = CreateGuiObject(new Vector2D(25, 5), @"\    \_\  \/ __ \|  Y Y  \  ___/  /    |    \   /\  ___/|  | \/");
			_gameOver5 = CreateGuiObject(new Vector2D(25, 6), @" \______  (____  /__|_|  /\___  > \_______  /\_/  \___  >__|   ");
			_gameOver6 = CreateGuiObject(new Vector2D(25, 7), @"        \/     \/      \/     \/          \/          \/       ");
			_headline = CreateGuiObject(new Vector2D(50, 9), $"Your Score: {_gameController.Score}");
			scores = ReadScores(_path).ToList();
			for (int i = 0; i < scores.Count; i++)
			{
				if (scores[i].Item2 <= _gameController.Score)
				{
					_placing = i;
					break;
				}
			}
			scores.Insert(_placing, new Tuple<string, int>("", _gameController.Score));
			for (int i = 0; i < 5; i++)
			{
				_top5.Add(CreateGuiObject(new Vector2D(52, 11 + i + (i+1)), $"{i + 1}.{scores[i].Item1}: {scores[i].Item2}"));
			}
			_timer.Start();
		}

		public override void Update()
		{
			if (Input.GetEnteredValue(out char c))
			{
				if (c == (char)13)
				{
					GameLevel.Clear = true;
					using (FileStream fs = new FileStream(_path, FileMode.Create))
					{
						using (StreamWriter sw = new StreamWriter(fs))
						{
							foreach (var score in scores)
							{
								sw.WriteLine($"{score.Item1};{score.Item2}");
							}
						}
					}

				}
				scores[_placing] = new Tuple<string, int>(scores[_placing].Item1 + c, scores[_placing].Item2);
			}
			if (_timer.IsBiggerThan(1000))
			{
				for (int i = 0; i < 5; i++)
				{
					_top5[i].Outputtext = $"{i + 1}.{scores[i].Item1}: {scores[i].Item2}";
					_top5[i].SendMessage("WriteUpdate", false, false);
				}
			}
		}

		private GuiObject CreateGuiObject(Vector2D start, string line)
		{
			var gui = GameLevel.AddNewGameObject<GuiObject>(0, 0);
			gui.TextStartingPostion = start;
			gui.BackgroundColor = ConsoleColor.Gray;
			gui.TextColor = ConsoleColor.Black;
			gui.Outputtext = line;
			gui.SendMessage("WriteUpdate", false, false);
			return gui;
		}

		public IEnumerable<Tuple<string, int>> ReadScores(string scoreFilepath)
		{
			List<Tuple<string, int>> scoreList = new List<Tuple<string, int>>();
			var scores = File.ReadAllLines(scoreFilepath);
			foreach (var score in scores)
			{
				scoreList.Add(new Tuple<string, int>(score.Split(';')[0], Convert.ToInt32(score.Split(';')[1])));
			}
			return scoreList.OrderByDescending(x => x.Item2);
		}
	}
}
