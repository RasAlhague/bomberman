﻿using Argis.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bomberman
{
	public class GameController : LevelObject
	{
		private Player _player;
		private readonly Timer _levelTime;

		public GuiObject ScoreUI { get; private set; }
		public GuiObject TimeUI { get; private set; }
		public GuiObject FpsUI { get; private set; }
		public GuiObject LevelNrUI { get; private set; }
		public int Score { get; private set; }

		public GameController(int x, int y, GameLevel gameLevel) : base(x, y, gameLevel)
		{
			Score = 0;
			_levelTime = new Timer();
		}

		public override void Start()
		{
			_levelTime.Reset();
			ScoreUI = GameLevel.AddNewGameObject<GuiObject>(0, 0);
			TimeUI = GameLevel.AddNewGameObject<GuiObject>(0, 0);
			FpsUI = GameLevel.AddNewGameObject<GuiObject>(0, 0);
			LevelNrUI = GameLevel.AddNewGameObject<GuiObject>(0, 0);

			LevelNrUI.TextStartingPostion = new Vector2D(10, 0);
			LevelNrUI.BackgroundColor = ConsoleColor.Gray;
			LevelNrUI.TextColor = ConsoleColor.Black;
			LevelNrUI.Outputtext = $"Level: {GameLevel.LevelName}";
			LevelNrUI.SendMessage("WriteUpdate", false, false);

			ScoreUI.TextStartingPostion = new Vector2D(40, 0);
			ScoreUI.BackgroundColor = ConsoleColor.Gray;
			ScoreUI.TextColor = ConsoleColor.Black;
			ScoreUI.Outputtext = $"Score: 0";
			ScoreUI.SendMessage("WriteUpdate", false, false);

			TimeUI.TextStartingPostion = new Vector2D(60, 0);
			TimeUI.BackgroundColor = ConsoleColor.Gray;
			TimeUI.TextColor = ConsoleColor.Black;
			TimeUI.Outputtext = $"Time: {_levelTime.TimeInMs / 1000}";
			TimeUI.SendMessage("WriteUpdate", false, false);

			FpsUI.TextStartingPostion = new Vector2D(80, 0);
			FpsUI.BackgroundColor = ConsoleColor.Gray;
			FpsUI.TextColor = ConsoleColor.Black;
			FpsUI.Outputtext = $"FPS: {GameLevel.Game.Fps}";
			FpsUI.SendMessage("WriteUpdate", false, false);

			_player = null;
			_timer.Start();
		}

		public override void Update()
		{
			if (_player == null)
			{
				_player = GameLevel.GameObjects.OfType<Player>().FirstOrDefault();
			}
			if (_timer.IsBiggerThan(1000, true))
			{
				FpsUI.Outputtext = $"FPS: {GameLevel.Game.Fps}";
				FpsUI.SendMessage("WriteUpdate", false, false);

				if (_player != null)
				{
					Score = _player.Score.ScoreCount;
				}

				ScoreUI.Outputtext = $"Score: {Score}";
				ScoreUI.SendMessage("WriteUpdate", false, false);

				TimeUI.Outputtext = $"Time: {_levelTime.TimeInMs / 1000}";
				TimeUI.SendMessage("WriteUpdate", false, false);

				LevelNrUI.Outputtext = $"Level: {GameLevel.LevelName}";
				LevelNrUI.SendMessage("WriteUpdate", false, false);
			}
		}
	}
}
