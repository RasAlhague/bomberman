﻿using Argis.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bomberman
{
    class Explosion : DrawableObject
    {
        public Directions Direction { get; set; }
        private int counter;
        private int distance;

        public Explosion(int x, int y, GameLevel gameLevel)
            : base(x, y, gameLevel)
        {
            this.Collider.IsEnabled = true;
            DrawLayer = 0;
        }

        public override void Start()
        {
            _bgColor = ConsoleColor.Yellow;
            distance = 0;
			_timer.Start();
        }

        public override void Update()
        {
            if (_timer.IsBiggerThan(250, true))
            {
                if (distance < 2)
                {
                    Transform.Move(Direction, 1);
                    NeedsRedraw = true;
                    distance++;
                }
                else
                {
                    Destroy(this);
                }
            }
        }

        public override void OnCollision(Collider other)
        {
			if(other.Transform.GameObject is Obstacle)
			{

			}
            else if (other.Transform.GameObject is Wall)
            {
                Destroy(this);
            }
        }
    }
}
