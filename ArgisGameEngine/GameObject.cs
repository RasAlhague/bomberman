﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Argis.Core
{
	/// <summary>
	/// 
	/// </summary>
	public abstract class GameObject
	{
		/// <summary>
		/// 
		/// </summary>
		public Transform Transform { get; }
		/// <summary>
		/// 
		/// </summary>
		public Collider Collider { get; }
		/// <summary>
		/// 
		/// </summary>
		public GameLevel GameLevel { get; }
		/// <summary>
		/// 
		/// </summary>
		public List<GameObject> Children { get; }
		/// <summary>
		/// 
		/// </summary>
		public bool IsActiv { get; set; }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="gameLevel"></param>
		protected GameObject(int x, int y, GameLevel gameLevel)
		{
			Transform = new Transform(x, y, this);
			GameLevel = gameLevel;
			Collider = new Collider(Transform, gameLevel)
			{
				IsEnabled = false
			};
			Children = new List<GameObject>();
			IsActiv = false;
		}
		/// <summary>
		/// 
		/// </summary>
		public abstract void Start();
		/// <summary>
		/// 
		/// </summary>
		public abstract void Update();
		/// <summary>
		/// 
		/// </summary>
		public abstract void OnDestroy();
		/// <summary>
		/// 
		/// </summary>
		/// <param name="other"></param>
		public virtual void OnCollision(Collider other)
		{
			// Gezielt, damit diese hier wenn sie nichts machen soll nicht im ziel überladen werden muss
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="other"></param>
		public virtual void OnTrigger(Collider other)
		{
			// Gezielt, damit diese hier wenn sie nichts machen soll nicht im ziel überladen werden muss
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="gameObject"></param>
		public void Destroy(GameObject gameObject)
		{
			GameLevel.DestroyTargets.Enqueue(gameObject);
		}
		public void SendMessage(string methodName, params object[] parameters)
		{
			try
			{
				MethodInfo methodInfo = this.GetType().GetMethod(methodName);
				methodInfo.Invoke(this, parameters);
			}
			catch (Exception ex)
			{
				// TODO: Hier Logging einfügen
			}
		}
		public void SendMessage(string methodName, bool needsReceiver, params object[] parameters)
		{
			try
			{
				MethodInfo methodInfo = this.GetType().GetMethod(methodName);
				if (!needsReceiver && methodInfo == null)
				{
					return;
				}
				methodInfo.Invoke(this, parameters);
			}
			catch (Exception ex)
			{
				// TODO: Hier Logging einfügen
			}
		}
	}
}