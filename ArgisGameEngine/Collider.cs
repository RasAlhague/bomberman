﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Argis.Core
{
	/// <summary>
	/// Klasse zum erkennen von Kollisionen. Kann ebenfalls als Trigger der bei Kollisionen
	/// nur eine Methode aufruft genutzt werden. Jedes GameObject hat in der aktuellen version einen Collider,
	/// der nur Standardmäßig deaktiviert wurde über <see cref="IsEnabled"/>.
	/// </summary>
	/// <remarks>
	/// Wird in einer späteren Version vom GameObject entkoppelt.
	/// </remarks>
	public sealed class Collider
	{
		/// <summary>
		/// Transform, wird benötigt um die eigene Position zu kennen.
		/// </summary>
		public Transform Transform { get; }
		/// <summary>
		/// Level in dem das GameObject dem diese Instanz von Collider hinzugefügt wurde, plaziert ist.
		/// </summary>
		private readonly GameLevel _gameLevel;
		/// <summary>
		/// Gibt an ob der Collider aktiviert ist und zur kollisionsprüfung verwendet werden kann.
		/// </summary>
		public bool IsEnabled { get; set; }
		/// <summary>
		/// Gibt an ob der Collider sich wie ein Trigger verhalten soll.
		/// </summary>
		public bool IsTrigger { get; set; }
        /// <summary>
        /// Hier blöden Kommentar einfügen XD
        /// </summary>
        /// <param name="transform">Die <see cref="Core.Transform"/> des GameObjects dem der Collider angeängt wird.</param>
        /// <param name="gameLevel">Das <see cref="GameLevel"/> des GameObjects dem der Collider angeängt wird.</param>
        public Collider(Transform transform, GameLevel gameLevel)
		{
			Transform = transform;
			_gameLevel = gameLevel;
		}
		public bool CheckCollider(int xPosition, int yPosition)
		{
			// Holt sich das erste GameObject mit dem es Collidiert.
			var collidingObject = _gameLevel.GameObjects.Where(g => g.Transform.GameObject != this.Transform.GameObject)  // nimmt das eigene GameObject aus der suche raus
														.Where(g => g.Collider.IsEnabled)  // Filtert nach GameObjects mit aktivien Collidern
														.FirstOrDefault(g => g.Transform.X == xPosition && g.Transform.Y == yPosition);    // Hollt sich das erste GameObject, wenn eines an den angegebenen Koordinaten befindet
			if (collidingObject != null)
			{
				if (IsTrigger)
				{
					// Falls beim auslösen des Triggers etwas passieren soll.
					Transform.GameObject.OnTrigger(collidingObject.Collider);
					if (collidingObject.Collider.IsTrigger)
					{
						collidingObject.SendMessage("OnGetTriggered", false, Transform.GameObject);
					}
					else
					{
						collidingObject.SendMessage("OnReceiveCollision", false, Transform.GameObject);
					}
					return true;
				}
				else
				{
					if (collidingObject.Collider.IsTrigger)
					{
						// Falls eine Kollision etwas auslösen soll.
						Transform.GameObject.OnCollision(collidingObject.Collider);
						collidingObject.SendMessage("OnGetTriggered", false, Transform.GameObject);
					}
					else
					{
						// Setzt die Position zurück
						Transform.ResetPosition();
						// Falls eine Kollision etwas auslösen soll.
						Transform.GameObject.OnCollision(collidingObject.Collider);
						collidingObject.SendMessage("OnReceiveCollision", false, Transform.GameObject);
					}

					return true;
				}
			}
			return false;
		}
	}
}