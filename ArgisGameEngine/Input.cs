﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Argis.Core
{
	/// <summary>
	/// Klasse für das erhalten von eingaben. Statisch weil diese Klasse nicht instanziert werden muss.
	/// </summary>
	public static class Input
	{
		/// <summary>
		/// Letztes eingebenes Zeichen. Nullable, damit ich wenn keines eingeben wurde oder es schon mit <see cref="GetKeyEntered(ConsoleKey)"/>
		/// verwertet wurde null sein kann und anzeigen kann keine eingabe zu haben.
		/// </summary>
		private static ConsoleKey? _lastSign;
		/// <summary>
		/// Gibt zurück ob eine bestimmte Taste eingegeben wurde.
		/// </summary>
		/// <param name="key"></param>
		/// <returns>Ja wenn eingeben, nein wenn nicht.</returns>
		public static bool GetKeyEntered(ConsoleKey key)
		{
			if (Console.KeyAvailable)
			{
				_lastSign = Console.ReadKey(true).Key;  // Keine ausgabe in der console
				return IsKey(key);
			}
			else if (_lastSign != null)
			{
				return IsKey(key);
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// Prüft die eingabe
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		private static bool IsKey(ConsoleKey key)
		{
			if (key == _lastSign)
			{
				_lastSign = null;
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}