﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Argis.Core
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class Transform
	{
		/// <summary>
		/// 
		/// </summary>
		public int X { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public int Y { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public int PreviousX { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public int PreviousY { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public GameObject GameObject { get; }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="gameObject"></param>
		public Transform(int x, int y, GameObject gameObject)
		{
			X = x;
			Y = y;
			PreviousX = X;
			PreviousY = Y;
			GameObject = gameObject;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		public void SetPosition(int x, int y)
		{
			X = x;
			Y = y;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="directions"></param>
		/// <param name="distance"></param>
		public void Move(Directions directions, int distance)
		{
			for (int i = 0; i < distance; i++)
			{
				CalcNextStepWithDirection(directions);
				if (GameObject.Collider.IsEnabled)
				{
					GameObject.Collider.CheckCollider(X, Y);
				}
			}
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="directions"></param>
		private void CalcNextStepWithDirection(Directions directions)
		{
			switch (directions)
			{
				case Directions.Left:
					X--;
					break;
				case Directions.Right:
					X++;
					break;
				case Directions.Up:
					Y--;
					break;
				case Directions.Down:
					Y++;
					break;
				case Directions.LeftUp:
					X--;
					Y--;
					break;
				case Directions.RightUp:
					X++;
					Y--;
					break;
				case Directions.LeftDown:
					X--;
					Y++;
					break;
				case Directions.RightDown:
					X++;
					Y++;
					break;
				default:
					break;
			}
		}
		/// <summary>
		/// 
		/// </summary>
		public void ResetPosition()
		{
			X = PreviousX;
			Y = PreviousY;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="directions"></param>
		/// <param name="distance"></param>
		public void MoveUncolliding(Directions directions, int distance)
		{
			switch (directions)
			{
				case Directions.Left:
					X -= distance;
					break;
				case Directions.Right:
					X += distance;
					break;
				case Directions.Up:
					Y -= distance;
					break;
				case Directions.Down:
					Y += distance;
					break;
				case Directions.LeftUp:
					X -= distance;
					Y -= distance;
					break;
				case Directions.RightUp:
					X += distance;
					Y -= distance;
					break;
				case Directions.LeftDown:
					X -= distance;
					Y += distance;
					break;
				case Directions.RightDown:
					X += distance;
					Y += distance;
					break;
				default:
					break;
			}
		}
		/// <summary>
		/// 
		/// </summary>
		public void FinishMove()
		{
			PreviousX = X;
			PreviousY = Y;
		}
	}
}