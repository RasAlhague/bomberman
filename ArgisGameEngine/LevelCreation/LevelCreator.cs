﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Argis.Core.LevelCreation
{
	public class LevelCreator
	{
		public GameLevel CreateLevelFromFile(string levelFile, string levelClassFile)
		{
			GameLevel gameLevel = new GameLevel(Path.GetFileNameWithoutExtension(levelFile));
			Dictionary<string, string> classNames = new Dictionary<string, string>();
			ParseClassesFile(levelClassFile, classNames);
			ParseLevelFile(levelFile, gameLevel, classNames);
			return gameLevel;
		}
		private void ParseClassesFile(string levelClassFile, Dictionary<string, string> classNames)
		{
			if (File.Exists(levelClassFile))
			{
				string[] classEntries = File.ReadAllLines(levelClassFile);
				foreach (var entry in classEntries)
				{
					string[] parts = entry.Split('=');
					classNames.Add(parts[0], parts[1]);
				}
			}
		}
		private void ParseLevelFile(string levelFile, GameLevel gameLevel, Dictionary<string, string> classNames)
		{
			Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
			if (File.Exists(levelFile))
			{
				List<LevelFileObject> objects = new List<LevelFileObject>();
				string[] fileContent = File.ReadAllLines(levelFile);
				for (int x = 0; x < fileContent.Length; x++)
				{
					for (int y = 0; y < fileContent[x].Length; y++)
					{
						objects.Add(new LevelFileObject() { Sign = fileContent[x][y].ToString(), X = y, Y = x });
					}
				}
				foreach (var key in classNames.Keys)
				{
					foreach (var item in objects.Where(x => x.Sign == key))
					{
						var name = classNames[item.Sign];
						foreach (Assembly assembly in assemblies)
						{
							Type type = assembly.GetType(name);
							if (type != null)
							{
								gameLevel.AddNewGameObject(type, item.X, item.Y);
							}
						}
					}
				}
			}
		}
	}
}