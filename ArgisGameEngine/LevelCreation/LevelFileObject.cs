﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Argis.Core.LevelCreation
{
	public class LevelFileObject
	{
		public string Sign { get; set; }
		public int X { get; set; }
		public int Y { get; set; }
	}
}
