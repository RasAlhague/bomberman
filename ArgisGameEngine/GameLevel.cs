﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Argis.Core
{
    /// <summary>
    /// Ein Konkretes Level im Spiel.
    /// </summary>
    public sealed class GameLevel
    {
        /// <summary>
        /// Die Objekte im Level.
        /// </summary>
        public List<GameObject> GameObjects { get; }
        /// <summary>
        /// Der Name des Levels.
        /// </summary>
        public string LevelName { get; private set; }
        /// <summary>
        /// Die algemeine hintergrundfarbe der Console.
        /// </summary>
        public ConsoleColor SkyboxColor { get; set; }
        /// <summary>
        /// GameObjects, die Zerstört werden sollen.
        /// </summary>
        public Queue<GameObject> DestroyTargets { get; }
        /// <summary>
        /// Das Spiel zu der das Level gehört.
        /// </summary>
        public Game Game { get; set; }
        /// <summary>
        /// Gibt an ob das Level durchgespielt wurde.
        /// </summary>
        public bool Clear { get; private set; }
        /// <summary>
        /// Konstruktor zur erzeugung ohne vorher das <see cref="Core.Game"/> kennen zu müssen.
        /// </summary>
        /// <param name="name"></param>
        public GameLevel(string name) : this(name, null)
        {
        }
        /// <summary>
        /// Konstruktor zur erzeugung mit <see cref="Core.Game"/>.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="game"></param>
        public GameLevel(string name, Game game)
        {
            Clear = false;
            LevelName = name;
            GameObjects = new List<GameObject>();
            DestroyTargets = new Queue<GameObject>();
            Game = game;
        }
        /// <summary>
        /// Ruft für alle <see cref="GameObject"/> in <see cref="GameObjects"/> die <see cref="GameObject.Update"/>
        /// </summary>
        public void Update()
        {
            for (int i = 0; i < GameObjects.Count; i++)
            {
                GameObjects[i].Update();
            }
        }
        /// <summary>
        /// Ruft für alle <see cref="DrawableObject"/> in <see cref="GameObjects"/> die <see cref="DrawableObject.Draw"/> Methode auf.
        /// </summary>
        public void Draw()
        {
            var drawObjects = GameObjects.OfType<DrawableObject>()
                                         .Where(x => x.IsActiv)
                                         .OrderBy(x => x.DrawLayer);
            foreach (var gameObject in drawObjects)
            {
                gameObject.Draw();
            }
        }
        /// <summary>
        /// Ruft für alle <see cref="GameObject"/> in <see cref="GameObjects"/> die <see cref="GameObject.Start"/> Methode auf.
        /// </summary>
        public void Start()
        {
            foreach (var gameObject in GameObjects.Where(x => !x.IsActiv))
            {
                gameObject.Start();
                gameObject.IsActiv = true;
            }
        }
        /// <summary>
        /// Ruft für alle <see cref="GameObject"/> in <see cref="GameObjects"/> die <see cref="Transform.FinishMove"/> Methode auf.
        /// </summary>
        public void FinalizeIteration()
        {
            foreach (var gameObject in GameObjects)
            {
                gameObject.Transform.FinishMove();
            }
        }
        /// <summary>
        /// Ruft für alle <see cref="GameObject"/> in <see cref="DestroyTargets"/> die <see cref="GameObject.OnDestroy"/> Methode auf und entfernt diese danach.
        /// </summary>
        public void Destroy()
        {
            while (DestroyTargets.Count != 0)
            {
                var gameObject = DestroyTargets.Dequeue();
                gameObject.OnDestroy();
                GameObjects.Remove(gameObject);
                //Type t = Type.GetType();
            }
        }
        public T AddNewGameObject<T>(int x, int y) where T : GameObject
		{
            var gameObject = (T)Activator.CreateInstance(typeof(T), x, y, this);
            GameObjects.Add(gameObject);
            return gameObject;

        }
		public GameObject AddNewGameObject(Type t, int x, int y)
		{
            GameObject gameObject = (GameObject)Activator.CreateInstance(t, x, y, this);
            GameObjects.Add(gameObject);
            return gameObject;
		}
	}
}