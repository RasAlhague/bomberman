﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Argis.Core
{
	public sealed class Raycast
	{
		private GameLevel _level;
		private int _x;
		private int _y;
        private GameObject gameObject;

		public Raycast(GameObject gameObject)
		{
			_level = gameObject.GameLevel;
            this.gameObject = gameObject;
			_x = gameObject.Transform.X;
			_y = gameObject.Transform.Y;
		}

		public bool ShootRay(Directions direction, int distance, out Transform transform)
		{
            _x = gameObject.Transform.X;
            _y = gameObject.Transform.Y;

            for (int i = 0; i < distance; i++)
			{
				CalcNextStepWithDirection(direction);
				// Holt sich das erste GameObject mit dem es Collidiert.
				var collidingObject = _level.GameObjects.Where(g => g.Collider.IsEnabled)  // Filtert nach GameObjects mit aktivien Collidern
														.FirstOrDefault(g => g.Transform.X == _x && g.Transform.Y == _y);    // Hollt sich das erste GameObject, wenn eines an den angegebenen Koordinaten befindet

				if(collidingObject != null)
				{
					transform = collidingObject.Transform;

					return true;
				}
			}
			transform = null;

			return false;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="directions"></param>
		private void CalcNextStepWithDirection(Directions directions)
		{
			switch (directions)
			{
				case Directions.Left:
					_x--;
					break;
				case Directions.Right:
					_x++;
					break;
				case Directions.Up:
					_y--;
					break;
				case Directions.Down:
					_y++;
					break;
				case Directions.LeftUp:
					_x--;
					_y--;
					break;
				case Directions.RightUp:
					_x++;
					_y--;
					break;
				case Directions.LeftDown:
					_x--;
					_y++;
					break;
				case Directions.RightDown:
					_x++;
					_y++;
					break;
				default:
					break;
			}
		}
	}
}
