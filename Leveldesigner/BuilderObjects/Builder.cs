﻿using Argis.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Leveldesigner.BuilderObjects
{
	class Builder : DrawableObject
	{
		private int _gameObjectIndex = 0;
		private bool _autoRefresh = true;
		private bool _continuePlanting = false;

		public List<Type> PluginTypes { get; }

		public Builder(int x, int y, GameLevel gameLevel) : base(x, y, gameLevel)
		{
			PluginTypes = new List<Type>();
		}

		public override void Start()
		{
			_bgColor = ConsoleColor.Yellow;
			_fgColor = ConsoleColor.Red;
			_sign = '$';
		}

		public override void Update()
		{
			if (Input.GetKeyEntered(ConsoleKey.W))
			{
				Transform.Move(Directions.Up, 1);
				BuilderRoutine();
			}
			else if (Input.GetKeyEntered(ConsoleKey.A))
			{
				Transform.Move(Directions.Left, 1);
				BuilderRoutine();
			}
			else if (Input.GetKeyEntered(ConsoleKey.S))
			{
				Transform.Move(Directions.Down, 1);
				BuilderRoutine();
			}
			else if (Input.GetKeyEntered(ConsoleKey.D))
			{
				Transform.Move(Directions.Right, 1);
				BuilderRoutine();
			}
			else if(Input.GetKeyEntered(ConsoleKey.N))
			{
				if(_gameObjectIndex++ >= PluginTypes.Count)
				{
					_gameObjectIndex = 0;
				}
			}
			else if(Input.GetKeyEntered(ConsoleKey.P))
			{
				GameLevel.AddNewGameObject(PluginTypes[_gameObjectIndex], Transform.X, Transform.Y);
				RefreshDrawAll();
			}
			else if (Input.GetKeyEntered(ConsoleKey.O))
			{
				_continuePlanting = !_continuePlanting;
			}
			else if(Input.GetKeyEntered(ConsoleKey.R))
			{
				_autoRefresh = !_autoRefresh;
			}
		}

		private void BuilderRoutine()
		{
			NeedsRedraw = true;

			if (_continuePlanting)
			{
				GameLevel.AddNewGameObject(PluginTypes[_gameObjectIndex], Transform.X, Transform.Y);
			}
			if (_autoRefresh)
			{
				RefreshDrawAll();
			}
		}

		private void RefreshDrawAll()
		{
			foreach (var gameObject in GameLevel.GameObjects.OfType<DrawableObject>())
			{
				gameObject.NeedsRedraw = true;
			}
		}
	}
}
