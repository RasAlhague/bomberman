﻿using Argis.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Leveldesigner
{
	class Program
	{
		static void Main(string pathToPluginFolder = "./plugins/")
		{
			string[] dllFileNames = GetDllFileNames(pathToPluginFolder);
			List<Assembly> assemblies = GetAssemblies(dllFileNames);
			List<Type> pluginTypes = GetPluginTypes(assemblies);

			Console.WriteLine("Leveldesigner:");
			Console.WriteLine("--------------------------------------------------------\n");

			Console.WriteLine("New Level:	<1>");
			Console.WriteLine("Load Level:	<2>");

			Console.Write(">>");
			string input = Console.ReadLine();

			switch (input)
			{
				case "1":
					Console.Clear();
					Game game = new Game();

					GameLevel gameLevel = new GameLevel("Test");
					gameLevel.AddNewGameObject<BuilderObjects.Builder>(0, 0);
					((BuilderObjects.Builder)gameLevel.GameObjects[0]).PluginTypes.AddRange(pluginTypes);

					game.AddLevel(gameLevel);

					game.Start();
					break;
				case "2":
					break;
				default:
					Console.WriteLine("Wrong...");
					break;
			}

			//var plugins = new List<GameObject>(pluginTypes.Count);
			//foreach (Type type in pluginTypes)
			//{
			//	GameObject plugin = (GameObject)Activator.CreateInstance(type);
			//	plugins.Add(plugin);
			//}
		}

		private static void BuildNewLevel()
		{
			Game game = new Game();
		}

		private static List<Type> GetPluginTypes(List<Assembly> assemblies)
		{
			Type pluginType = typeof(GameObject);
			var pluginTypes = new List<Type>();
			foreach (Assembly assembly in assemblies)
			{
				if (assembly != null)
				{
					Type[] types = assembly.GetTypes();
					foreach (Type type in types)
					{
						if (type.IsInterface || type.IsAbstract)
						{
							continue;
						}
						else
						{
							if (type.IsSubclassOf(pluginType))
							{
								pluginTypes.Add(type);
							}
						}
					}
				}
			}

			return pluginTypes;
		}

		private static List<Assembly> GetAssemblies(string[] dllFileNames)
		{
			var assemblies = new List<Assembly>(dllFileNames.Length);
			foreach (string dllFile in dllFileNames)
			{
				//AssemblyName an = AssemblyName.GetAssemblyName(dllFile);
				Assembly assembly = Assembly.LoadFrom(dllFile);
				assemblies.Add(assembly);
			}

			return assemblies;
		}

		private static string[] GetDllFileNames(string pathToPluginFolder)
		{
			string[] dllFileNames = null;
			if (Directory.Exists(pathToPluginFolder))
			{
				dllFileNames = Directory.GetFiles(pathToPluginFolder, "*.dll");
			}

			return dllFileNames;
		}
	}
}
