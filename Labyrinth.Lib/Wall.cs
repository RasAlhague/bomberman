﻿using Argis.Core;
using System;

namespace Labyrinth.Lib
{
	public class Wall : DrawableObject
	{
		public Wall(int x, int y, GameLevel gameLevel) : base(x, y, gameLevel)
		{
			Collider.IsEnabled = true;
			Collider.IsTrigger = false;
			DrawLayer = 5;
		}
		public override void Start()
		{
			_bgColor = ConsoleColor.DarkBlue;
			_fgColor = ConsoleColor.White;
			_sign = '#';
		}
		public override void Update()
		{
		}
	}
}
