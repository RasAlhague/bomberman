﻿using Argis.Core;
using System;

namespace Bomberman
{
	public class Bomb : DrawableObject
	{
        public Enemy Origin { get; set; }
        private int counter = 0;
        private Raycast raycast;

        public Bomb(int x, int y, GameLevel gameLevel) : base(x, y, gameLevel)
		{
			Collider.IsEnabled = true;
            raycast = new Raycast(this);
            DrawLayer = 0;
		}
		public override void Start()
		{
			_bgColor = ConsoleColor.Red;
			_fgColor = ConsoleColor.White;
			_sign = 'B';
		}
		public override void Update()
		{
            counter++;
            if(counter % 33 == 0)
            {
                _sign = Convert.ToChar((1000 / counter));
                NeedsRedraw = true;
            }
            if(counter == 1000)
            {
                SpawnExplosion(Directions.Left, Transform.X - 1, Transform.Y);

                SpawnExplosion(Directions.Right, Transform.X + 1, Transform.Y);

                SpawnExplosion(Directions.Up, Transform.X, Transform.Y - 1);

                SpawnExplosion(Directions.Down, Transform.X, Transform.Y + 1);

                Destroy(this);
            }
            
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            if(Origin != null)
            {
                Origin.BombLock = false;
            }
            
        }

        private void SpawnExplosion(Directions direction,int x, int y) {
            if(raycast.ShootRay(direction,1,out Transform transform)){
                transform.GameObject.SendMessage("OnReceiveCollision", false, this);
            }
            else
            {
                Explosion explosion = GameLevel.AddNewGameObject<Explosion>(x, y);
                explosion.Direction = direction;
            }
        }


	}
}
