﻿using Argis.Core;

namespace Bomberman
{
	public static class Program
	{
		static void Main(string[] args)
		{
			Argis.Core.LevelCreation.LevelCreator levelCreator = new Argis.Core.LevelCreation.LevelCreator();
			Game game = new Game();
			game.AddLevel(levelCreator.CreateLevelFromFile(@"C:\Users\Jan\Documents\Schule\repos\bomberman\Bomberman\level\map.txt", @"C:\Users\Jan\Documents\Schule\repos\bomberman\Bomberman\level\classes.txt"));
			game.Start();
		}
	}
}