﻿using Argis.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bomberman
{
    public class Score : GameObject
    {
        public int ScoreCount { get; set; }

        public Score(int x, int y, GameLevel gameLevel) : base(x, y, gameLevel)
        {
        }

        public void IncreaseScore(int value)
        {
            ScoreCount+=value;
        }

        public override void OnDestroy()
        {
        }

        public override void Start()
        {
            ScoreCount = 0;
        }

        public override void Update()
        {
        }
    }
}
