﻿using Argis.Core;
using System;

namespace Bomberman
{
	public class Player : Enemy
	{
		public Player(int x, int y, GameLevel gameLevel) : base(x, y, gameLevel)
		{
            this.Collider.IsTrigger = false;
        }
		public override void Start()
		{
            base.Start();
			_fgColor = ConsoleColor.DarkGreen;
			_bgColor = ConsoleColor.DarkRed;
			_sign = 'P';
            scoreValue = 0;
		}
		public override void Update()
		{
            if (Input.GetKeyEntered(ConsoleKey.W))
            {
                Transform.Move(Directions.Up, 1);
                NeedsRedraw = true;
            }
            else if (Input.GetKeyEntered(ConsoleKey.A))
            {
                Transform.Move(Directions.Left, 1);
                NeedsRedraw = true;
            }
            else if (Input.GetKeyEntered(ConsoleKey.S))
            {
                Transform.Move(Directions.Down, 1);
                NeedsRedraw = true;
            }
            else if (Input.GetKeyEntered(ConsoleKey.D))
            {
                Transform.Move(Directions.Right, 1);
                NeedsRedraw = true;
            }
            else if (Input.GetKeyEntered(ConsoleKey.R) && BombLock == false)
            {
                Bomb bomb = GameLevel.AddNewGameObject<Bomb>(Transform.X, Transform.Y);
                bomb.Origin = this;
                BombLock = true;
                NeedsRedraw = true;
            }
		}



        public override void OnDestroy()
        {
            base.OnDestroy();
            // ToDo: Next Screen
        }
    }
}