﻿using Argis.Core;
using System;
using System.Linq;

namespace Bomberman
{
	public class Enemy : DrawableObject
	{
        public bool BombLock { get; set; }

        private readonly Random r;
		private int counter = 0;
        private Raycast raycast;
        protected int scoreValue;
        protected Score score;
        

		public Enemy(int x, int y, GameLevel gameLevel) : base(x, y, gameLevel)
		{
			this.Collider.IsEnabled = true;
			this.Collider.IsTrigger = false;
            raycast = new Raycast(this);
            score = GameLevel.GameObjects.OfType<Score>().FirstOrDefault();
            r = new Random();
			DrawLayer = 0;
		}
		public override void Start()
		{
			_bgColor = ConsoleColor.DarkMagenta;
			_fgColor = ConsoleColor.White;
            BombLock = false;
            scoreValue = 10;

        }
		public override void Update()
		{
			if (counter++ % 500 == 0)
			{
                Directions direction =  Directions.None;
                do
                {
                    direction = (Directions)r.Next(0, 4);
                } while (raycast.ShootRay(direction, 1, out Transform transform));
                

                Transform.Move(direction, 1);
				NeedsRedraw = true;
			}
		}

        public virtual void OnReceiveCollision(GameObject other)
        {
            Explosion explosion = other as Explosion;

            if (explosion != null)
            {
                Destroy(this);
            }
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            score?.IncreaseScore(scoreValue);
        }

    }
}
