﻿using Argis.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bomberman
{
    class Explosion : DrawableObject
    {
        public Directions Direction { get; set; }
        private int counter;
        private int distance;

        public Explosion(int x, int y, GameLevel gameLevel) : base(x, y, gameLevel)
        {
            this.Collider.IsEnabled = true;
            DrawLayer = 0;
        }

        public override void Start()
        {
            _bgColor = ConsoleColor.Yellow;
            distance = 0;
        }

        public override void Update()
        {
            if (counter++ % 100 == 0)
            {
                if (distance < 3)
                {
                    Transform.Move(Direction, 1);
                    NeedsRedraw = true;
                    distance++;
                }
                else
                {
                    Destroy(this);
                }
            }
        }

        public override void OnCollision(Collider other)
        {

            switch (other.Transform.GameObject)
            {
                case Wall wall:
                    Destroy(this);
                    break;
            }
        }
    }
}
