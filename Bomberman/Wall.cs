﻿using Argis.Core;
using System;

namespace Bomberman
{
	public class Wall : DrawableObject
	{
		public Wall(int x, int y, GameLevel gameLevel) : base(x, y, gameLevel)
		{
			Collider.IsEnabled = true;
			Collider.IsTrigger = false;
			DrawLayer = 5;
		}
		public override void Start()
		{
			_bgColor = ConsoleColor.DarkBlue;
		}
		public override void Update()
		{
		}
	}
}
