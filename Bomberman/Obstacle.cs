﻿using System;
using System.Collections.Generic;
using System.Text;
using Argis.Core;

namespace Bomberman
{
    class Obstacle : Wall
    {
        public Obstacle(int x, int y, GameLevel gameLevel) : base(x, y, gameLevel)
        {
        }

        public override void Start()
        {
            _bgColor = ConsoleColor.Gray;
        }

        public void OnReceiveCollision(GameObject other)
        {
            Explosion explosion = other as Explosion;

            if (explosion != null)
            {
                Destroy(this);
            }
        }
    }
}
